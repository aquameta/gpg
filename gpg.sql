begin;

drop schema if exists gpg cascade;
create schema gpg;
set search_path=gpg;


create table card (
    id serial primary key,
    label text,
    image_front bytea,
    image_back bytea
);

create table game (
id serial primary key,
topic text,
board_width integer
);

create table game_card (
id serial primary key,
card_id integer references card(id),
game_id integer references game(id),
cell integer
);

create table player (
id serial primary key,
name text,
game_id integer references game(id)
-- user_id uuid references endpoint.user(id)
);

create table cube_placement (
id serial primary key,
player_id integer references player(id),
game_card_id integer references game_card(id)
);

CREATE TYPE cube_state AS ENUM ('ok', 'permit', 'challenge', 'no_connect', 'number');

create table turn (
id serial primary key,
cube_state cube_state
);



insert into card (label) values
('gestalt'),
('emotional manipulation'),
('coding'),
('ambivilance'),
('harmony'),
('helplessness'),
('fundamental theorem of calculus'),
('hidden potential'),
('creation'),
('freedom'),
('city as artifact')
;






commit;
